module.exports = {
    url: "http://127.0.0.1:8545",
    defaultPath: "m/44'/60'/0'/0/0",
    defaultPathForIndex: (index) => `m/44'/60'/0'/0/${index}`,
    exampleMnemonic: "edit control feature fade bring fabric prepare faint pepper rubber wish buyer",
    minConfirmationsRequired: 1,
}