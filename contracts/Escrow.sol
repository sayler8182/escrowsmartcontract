pragma solidity >=0.4.21 <0.6.0;

import '../node_modules/openzeppelin-solidity/contracts/ownership/Ownable.sol';

contract Escrow is Ownable {
    enum Status { INITIATED, PENDING, DISPUTE, CANCELLED, REJECTED, SUCCESS }
    struct EscrowStruct {
        address creator;
        address broker;
        address recipient;
        string notes;
        uint256 amount;
        uint256 brokerFee;
        Status status;
    }

    /**
     * Available events
    */
    event EscrowStatusChanged(address indexed sender, Status oldStatus, Status newStatus);
    event EscrowPendingWithdrawsChanged(address indexed sender, uint amount);

    uint private balance;
    EscrowStruct private escrow;
    mapping(address => uint) private pendingWithdraws;

    /**
     * Allow access only for creators
    */
    modifier isCreatorOrBroker() {
        require(escrow.creator == msg.sender || escrow.broker == msg.sender, "Is neither creator nor broker");
        _;
    }

    /**
     * Allow access only for creator
    */
    modifier isCreator() {
        require(escrow.creator == msg.sender, "Is not creator");
        _;
    }

    /**
     * Allow access only for broker
    */
    modifier isBroker() {
        require(escrow.broker == msg.sender, "Is not broker");
        _;
    }

    /**
     * Compare status with current escrow status
    */
    modifier isStatus(Status status) {
        require(escrow.status == status, "Incorrect status");
        _;
    }

    /**
     * Check if escrow is done
     * escrow is done if is CANCELLED, REJECTED, SUCCESS
     * when escrow is done withdraws are enable
    */
    modifier isDone() {
        require(escrow.status == Status.CANCELLED || escrow.status == Status.REJECTED || escrow.status == Status.SUCCESS, "Is not done yet");
        _;
    }

    /**
     * Check if address has sufficient funds for withdraw
    */
    modifier isWithdrawable() {
        require(pendingWithdraws[msg.sender] > 0, "Insufficient funds");
        _;
    }

    /**
     * Create contract and start new escrow transaction
     * @param brokerAddress broker wallet address
     * @param recipientAddress recipient wallet address
     * @param fee fee for broker (<1, 5000> where 1 -> 0.01%, 5000 -> 50%)
     * @param notes escrow additional info (max 255 chars)
     * msg.value contains total transaction amount (amount end fee)
    */
    constructor(
        address brokerAddress,
        address recipientAddress,
        uint256 fee,
        string memory notes
        )
        public
        payable
        {

        require(msg.value > 0 && msg.value < 2**64, "Value is not in range (0, 2**64)");
        require(msg.sender != brokerAddress, "Creator and broker can't be the same");
        require(msg.sender != recipientAddress, "Creator and recipient can't be the same");
        require(fee >= 1 && fee <= 5000, "Fee is not in range <1, 5000>");
        require(bytes(notes).length < 256, "Notes is too long");

        // calculate
        uint256 brokerFee = fee * msg.value / 10000;
        uint256 amount = msg.value - brokerFee;

        // fill escrow
        EscrowStruct memory _escrow;
        _escrow.creator = msg.sender;
        _escrow.broker = brokerAddress;
        _escrow.recipient = recipientAddress;
        _escrow.brokerFee = brokerFee;
        _escrow.amount = amount;
        _escrow.notes = notes;
        _escrow.status = Status.INITIATED;

        // save escrow in contract
        escrow = _escrow;
    }

    /**
     * Escrow transaction details
    */
    function getDetails()
        public
        view
        returns (
            address creator,
            address broker,
            address recipient,
            string memory notes,
            uint brokerFee,
            uint amount,
            Status status
        ) {
            return (escrow.creator, escrow.broker, escrow.recipient, escrow.notes, escrow.brokerFee, escrow.amount, escrow.status);
    }

    /**
     * Escrow transaction status
     * INITIATED, PENDING, DISPUTE, CANCELLED, REJECTED, SUCCESS
    */
    function getStatus() public view returns (Status status) {
        return escrow.status;
    }

    /**
     * Accept escrow transaction
     * only available for creator or broker
     * ----
     * changes status from INITIATED to:
     * - SUCCESS for creator
     * - PENDING for broker
     * ----
     * sets withdraws
     * - creator: 0
     * - broker: brokerFee
     * - recipient: amount
    */
    function accept() public isCreatorOrBroker() {
        if (escrow.creator == msg.sender) {
            (Status oldStatus, Status newStatus) = _setStatus(Status.INITIATED, Status.SUCCESS);
            pendingWithdraws[escrow.creator] = escrow.brokerFee;
            pendingWithdraws[escrow.recipient] = escrow.amount;
            emit EscrowPendingWithdrawsChanged(escrow.creator, escrow.brokerFee);
            emit EscrowPendingWithdrawsChanged(escrow.recipient, escrow.amount);
            emit EscrowStatusChanged(msg.sender, oldStatus, newStatus);
        } else if (escrow.broker == msg.sender) {
            (Status oldStatus, Status newStatus) = _setStatus(Status.INITIATED, Status.PENDING);
            emit EscrowStatusChanged(msg.sender, oldStatus, newStatus);
        }
    }

    /**
     * Cancel escrow transaction
     * only available for creator or broker
     * ----
     * changes status from INITIATED to CANCELLED
     * ----
     * sets withdraws
     * - creator: amount + brokerFee
     * - broker: 0
     * - recipient: 0
    */
    function cancel() public isCreatorOrBroker() {
        (Status oldStatus, Status newStatus) = _setStatus(Status.INITIATED, Status.CANCELLED);
        pendingWithdraws[escrow.creator] = escrow.amount + escrow.brokerFee;
        emit EscrowPendingWithdrawsChanged(escrow.creator, escrow.amount + escrow.brokerFee);
        emit EscrowStatusChanged(msg.sender, oldStatus, newStatus);
    }

    /**
     * Confirm escrow transaction
     * only available for creator or broker
     * ----
     * changes status from PENDING to SUCCESS
     * ----
     * sets withdraws
     * - creator: 0
     * - broker: brokerFee
     * - recipient: amount
    */
    function confirm() public isCreatorOrBroker() {
        (Status oldStatus, Status newStatus) = _setStatus(Status.PENDING, Status.SUCCESS);
        pendingWithdraws[escrow.broker] = escrow.brokerFee;
        pendingWithdraws[escrow.recipient] = escrow.amount;
        emit EscrowPendingWithdrawsChanged(escrow.creator, escrow.brokerFee);
        emit EscrowPendingWithdrawsChanged(escrow.recipient, escrow.amount);
        emit EscrowStatusChanged(msg.sender, oldStatus, newStatus);
    }

    /**
     * Dispute escrow transaction
     * only available for creator
     * ----
     * changes status from PENDING to DISPUTE
     * ----
     * don't sets withdraws
    */
    function dispute() public isCreator() {
        (Status oldStatus, Status newStatus) = _setStatus(Status.PENDING, Status.DISPUTE);
        emit EscrowStatusChanged(msg.sender, oldStatus, newStatus);

    }

    /**
     * Confirm dispute escrow transaction
     * only available for creator or broker
     * ----
     * changes status from DISPUTE to SUCCESS
     * ----
     * sets withdraws
     * - creator: 0
     * - broker: brokerFee
     * - recipient: amount
    */
    function confirmDispute() public isCreatorOrBroker() {
        (Status oldStatus, Status newStatus) = _setStatus(Status.DISPUTE, Status.SUCCESS);
        pendingWithdraws[escrow.broker] = escrow.brokerFee;
        pendingWithdraws[escrow.recipient] = escrow.amount;
        emit EscrowPendingWithdrawsChanged(escrow.broker, escrow.brokerFee);
        emit EscrowPendingWithdrawsChanged(escrow.recipient, escrow.amount);
        emit EscrowStatusChanged(msg.sender, oldStatus, newStatus);
    }

    /**
     * Reject dispute escrow transaction
     * only available for broker
     * ----
     * changes status from DISPUTE to REJECTED
     * ----
     * sets withdraws
     * - creator: amount
     * - broker: brokerFee
     * - recipient: 0
    */
    function rejectDispute() public isBroker() {
        (Status oldStatus, Status newStatus) = _setStatus(Status.DISPUTE, Status.REJECTED);
        pendingWithdraws[escrow.creator] = escrow.amount;
        pendingWithdraws[escrow.broker] = escrow.brokerFee;
        emit EscrowPendingWithdrawsChanged(escrow.creator, escrow.amount);
        emit EscrowPendingWithdrawsChanged(escrow.broker, escrow.brokerFee);
        emit EscrowStatusChanged(msg.sender, oldStatus, newStatus);
    }

    /**
     * Available withdraw details for current address
     * only available when escrow is done
    */
    function availableWithdraw() public view isDone() returns (uint amount) {
        return pendingWithdraws[msg.sender];
    }

    /**
     * Withdraw funds to wallet
     * only available when escrow is done and there are some funds to withdraw
    */
    function withdraw() public payable isDone() isWithdrawable() {
        uint amount = pendingWithdraws[msg.sender];
        pendingWithdraws[msg.sender] = 0;
        msg.sender.transfer(amount);
    }

    /**
     * Set status for current escrow
    */
    function _setStatus(
        Status _requireStatus,
        Status _newStatus
        )
        private
        isStatus(_requireStatus)
        returns (Status oldStatus, Status newStatus)
        {
        oldStatus = escrow.status;
        newStatus = _newStatus;
        escrow.status = _newStatus;
        return (oldStatus, newStatus);
    }
}