const dotenv = require("dotenv");
dotenv.config();

module.exports = {
    compilers: {
        solc: {
            version: "^0.5.2",
        },
    },
    networks: {
        ganache: {
            host: process.env.LOCAL_NETWORK_HOST,
            port: process.env.LOCAL_NETWORK_PORT,
            network_id: "*",
        },
    },
};
