const assert = require("assert");
const { Escrow, STATUS_NAME } = require("../src/Escrow");
const {
    creatorWallet,
    brokerWallet,
    recipientWallet,
} = require("./config");
const {
    startEscrow,
    openNewDefaultEscrow,
    openEscrow,
    checkContractState,
    amountWithoutFee,
    amountWithFee,
    amountFee,
} = require("./utils");

// escrow
let escrow = null;

// start tests
describe("Contract", () => {

    // start escrow
    context("Start Escrow", () => {
        it("Create new Escrow", async () => {
            const creatorEscrow = new Escrow(creatorWallet);
            assert.ok(creatorEscrow, "Escrow has not been created");
            assert.equal(creatorEscrow._contract, null, "Contract should not be set");
            assert.ok(creatorEscrow._wallet, "Wallet should be set");
        });

        it("Create new Escrow Transaction", async () => {
            escrow = await startEscrow(creatorWallet, {
                recipientAddress: recipientWallet.getAddress(),
                brokerAddress: brokerWallet.getAddress(),
                amount: 1,
                fee: 1,
                notes: "Some note",
            });
            assert.ok(escrow._contract, "Escrow has not been created");
            assert.ok(escrow.getAddress(), "Escrow has no address");
        });

        it("Create Escrow from contract", async () => {
            const contractAddress = escrow.getAddress();
            const creatorEscrow = new Escrow(creatorWallet, { contractAddress });
            assert.ok(creatorEscrow, "Escrow has not been created");
            assert.ok(creatorEscrow._contract, "Contract should be set");
            assert.ok(creatorEscrow._wallet, "Wallet should be set");
        });

        it("Get Escrow Transaction Details", async () => {
            const details = await escrow.getDetails();
            assert.equal(details.creator, creatorWallet.getAddress(), "Incorrect creator");
            assert.equal(details.recipient, recipientWallet.getAddress(), "Incorrect recipient");
            assert.equal(details.broker, brokerWallet.getAddress(), "Incorrect broker");
            assert.equal(details.notes, "Some note", "Incorrect notes");
            assert.equal(details.brokerFee, "0.01", "Incorrect brokerFee");
            assert.equal(details.amount, "0.99", "Incorrect amount");
            assert.equal(details.status, STATUS_NAME.INITIATED, "Incorrect status");
        });

        it("Get Escrow Transaction Status", async () => {
            const status = await escrow.getStatus();
            assert.equal(status, STATUS_NAME.INITIATED);
        })
    });
});

describe("Creator flow", () => {
    // Accept creator path
    context("Start Escrow and ACCEPT by CREATOR", async () => {
        it("New transaction", async () => {
            escrow = await openNewDefaultEscrow(creatorWallet, creatorWallet);
        })

        // Creator ACCEPT
        it("Accept by creator", async () => {
            const statusBefore = await escrow.getStatus();
            await escrow.accept();
            const statusAfter = await escrow.getStatus();
            assert.equal(statusBefore, STATUS_NAME.INITIATED, `Incorrect previous status -> ${statusBefore}`);
            assert.equal(statusAfter, STATUS_NAME.SUCCESS, `Incorrect new status -> ${statusAfter}`);
            await checkContractState(escrow, {
                creator: { amount: amountFee() },
                broker: { amount: 0 },
                recipient: { amount: amountWithoutFee() },
            });
        });
    });

    // Cancel creator path
    context("Start Escrow and CANCEL by CREATOR", async () => {
        it("New transaction", async () => {
            escrow = await openNewDefaultEscrow(creatorWallet, creatorWallet);
        })

        // Creator CANCEL
        it("Cancel by creator", async () => {
            const statusBefore = await escrow.getStatus();
            await escrow.cancel();
            const statusAfter = await escrow.getStatus();
            assert.equal(statusBefore, STATUS_NAME.INITIATED, `Incorrect previous status -> ${statusBefore}`);
            assert.equal(statusAfter, STATUS_NAME.CANCELLED, `Incorrect new status -> ${statusAfter}`);
            await checkContractState(escrow, {
                creator: { amount: amountWithFee() },
                broker: { amount: 0 },
                recipient: { amount: 0 },
            });
        });
    });

    // Confirm creator path
    context("Start Escrow and ACCEPT -> CONFIRM by CREATOR", async () => {
        it("New transaction", async () => {
            escrow = await openNewDefaultEscrow(creatorWallet, creatorWallet);
        })

        // Broker ACCEPT
        // Creator CONFIRM
        it("Confirm by creator", async () => {
            const brokerEscrow = openEscrow(brokerWallet, escrow)
            await brokerEscrow.accept();
            const statusBefore = await escrow.getStatus();
            await escrow.confirm();
            const statusAfter = await escrow.getStatus();
            assert.equal(statusBefore, STATUS_NAME.PENDING, `Incorrect previous status -> ${statusBefore}`);
            assert.equal(statusAfter, STATUS_NAME.SUCCESS, `Incorrect new status -> ${statusAfter}`);
            await checkContractState(escrow, {
                creator: { amount: 0 },
                broker: { amount: amountFee() },
                recipient: { amount: amountWithoutFee() },
            });
        });
    })

    // Dispute creator path
    context("Start Escrow and ACCEPT -> DISPUTE by CREATOR", async () => {
        it("New transaction", async () => {
            escrow = await openNewDefaultEscrow(creatorWallet, creatorWallet);
        })

        // Broker ACCEPT
        // Creator DISPUTE
        it("Dispute by creator", async () => {
            const brokerEscrow = openEscrow(brokerWallet, escrow)
            await brokerEscrow.accept();
            const statusBefore = await escrow.getStatus();
            await escrow.dispute();
            const statusAfter = await escrow.getStatus();
            assert.equal(statusBefore, STATUS_NAME.PENDING, `Incorrect previous status -> ${statusBefore}`);
            assert.equal(statusAfter, STATUS_NAME.DISPUTE, `Incorrect new status -> ${statusAfter}`);
            await checkContractState(escrow, {
                creator: { confirmDispute: true, },
                broker: { confirmDispute: true, rejectDispute: true },
            });
        });
    })

    // Confirm dispute creator path
    context("Start Escrow and ACCEPT -> DISPUTE -> CONFIRM_DISPUTE by CREATOR", async () => {
        it("New transaction", async () => {
            escrow = await openNewDefaultEscrow(creatorWallet, creatorWallet);
        })

        // Broker ACCEPT
        // Creator DISPUTE
        // Creator CONFIRM_DISPUTE
        it("Confirm dispute by creator", async () => {
            const brokerEscrow = openEscrow(brokerWallet, escrow)
            await brokerEscrow.accept();
            await escrow.dispute();
            const statusBefore = await escrow.getStatus();
            await escrow.confirmDispute();
            const statusAfter = await escrow.getStatus();
            assert.equal(statusBefore, STATUS_NAME.DISPUTE, `Incorrect previous status -> ${statusBefore}`);
            assert.equal(statusAfter, STATUS_NAME.SUCCESS, `Incorrect new status -> ${statusAfter}`);
            await checkContractState(escrow, {
                creator: { amount: 0 },
                broker: { amount: amountFee() },
                recipient: { amount: amountWithoutFee() },
            });
        });
    })
});

describe("Broker flow", () => {
    // Accept broker path
    context("Start Escrow and ACCEPT by BROKER", async () => {
        it("New transaction", async () => {
            escrow = await openNewDefaultEscrow(creatorWallet, brokerWallet);
        })

        // Broker ACCEPT
        it("Accept by broker", async () => {
            const statusBefore = await escrow.getStatus();
            await escrow.accept();
            const statusAfter = await escrow.getStatus();
            assert.equal(statusBefore, STATUS_NAME.INITIATED, `Incorrect previous status -> ${statusBefore}`);
            assert.equal(statusAfter, STATUS_NAME.PENDING, `Incorrect new status -> ${statusAfter}`);
            await checkContractState(escrow, {
                creator: { confirm: true, dispute: true },
                broker: { confirm: true },
            });
        });
    });

    // Cancel broker path
    context("Start Escrow and CANCEL by BROKER", async () => {
        it("New transaction", async () => {
            escrow = await openNewDefaultEscrow(creatorWallet, brokerWallet);
        })

        // Broker CANCEL
        it("Cancel by broker", async () => {
            const statusBefore = await escrow.getStatus();
            await escrow.cancel();
            const statusAfter = await escrow.getStatus();
            assert.equal(statusBefore, STATUS_NAME.INITIATED, `Incorrect previous status -> ${statusBefore}`);
            assert.equal(statusAfter, STATUS_NAME.CANCELLED, `Incorrect new status -> ${statusAfter}`);
            await checkContractState(escrow, {
                creator: { amount: amountWithFee() },
                broker: { amount: 0 },
                recipient: { amount: 0 },
            });
        });
    });

    // Confirm broker path
    context("Start Escrow and ACCEPT -> CONFIRM by BROKER", async () => {
        it("New transaction", async () => {
            escrow = await openNewDefaultEscrow(creatorWallet, brokerWallet);
        })

        // Broker ACCEPT
        // Broker CONFIRM
        it("Confirm by broker", async () => {
            await escrow.accept();
            const statusBefore = await escrow.getStatus();
            await escrow.confirm();
            const statusAfter = await escrow.getStatus();
            assert.equal(statusBefore, STATUS_NAME.PENDING, `Incorrect previous status -> ${statusBefore}`);
            assert.equal(statusAfter, STATUS_NAME.SUCCESS, `Incorrect new status -> ${statusAfter}`);
            await checkContractState(escrow, {
                creator: { amount: 0 },
                broker: { amount: amountFee() },
                recipient: { amount: amountWithoutFee() },
            });
        });
    })

    // Confirm dispute broker path
    context("Start Escrow and ACCEPT -> DISPUTE -> CONFIRM_DISPUTE by BROKER", async () => {
        it("New transaction", async () => {
            escrow = await openNewDefaultEscrow(creatorWallet, brokerWallet);
        })

        // Broker ACCEPT
        // Creator DISPUTE
        // Broker CONFIRM_DISPUTE
        it("Confirm dispute by broker", async () => {
            const creatorEscrow = openEscrow(creatorWallet, escrow)
            await escrow.accept();
            await creatorEscrow.dispute();
            const statusBefore = await escrow.getStatus();
            await escrow.confirmDispute();
            const statusAfter = await escrow.getStatus();
            assert.equal(statusBefore, STATUS_NAME.DISPUTE, `Incorrect previous status -> ${statusBefore}`);
            assert.equal(statusAfter, STATUS_NAME.SUCCESS, `Incorrect new status -> ${statusAfter}`);
            await checkContractState(escrow, {
                creator: { amount: 0 },
                broker: { amount: amountFee() },
                recipient: { amount: amountWithoutFee() },
            });
        });
    })

    // Reject dispute broker path
    context("Start Escrow and ACCEPT -> DISPUTE -> REJECT_DISPUTE by BROKER", async () => {
        it("New transaction", async () => {
            escrow = await openNewDefaultEscrow(creatorWallet, brokerWallet);
        })

        // Broker ACCEPT
        // Creator DISPUTE
        // Broker REJECT_DISPUTE
        it("Reject dispute by broker", async () => {
            const creatorEscrow = openEscrow(creatorWallet, escrow)
            await escrow.accept();
            await creatorEscrow.dispute();
            const statusBefore = await escrow.getStatus();
            await escrow.rejectDispute();
            const statusAfter = await escrow.getStatus();
            assert.equal(statusBefore, STATUS_NAME.DISPUTE, `Incorrect previous status -> ${statusBefore}`);
            assert.equal(statusAfter, STATUS_NAME.REJECTED, `Incorrect new status -> ${statusAfter}`);
            await checkContractState(escrow, {
                creator: { amount: amountWithoutFee() },
                broker: { amount: amountFee() },
                recipient: { amount: 0 },
            });
        });
    })
}); 