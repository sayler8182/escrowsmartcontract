const { EscrowWallet } = require("../src/EscrowWallet");

const creatorWallet = new EscrowWallet({ privateKey: "53149cc9a397085f01a17c5b2c5638887ff930f3acdefeb2b2dbf95c7ee80c1d" });
const brokerWallet = new EscrowWallet({ privateKey: "2529a3c1eca4b73631726461b1dba5a9c21dac4dc7029c96140d434bc0fa6607" });
const recipientWallet = new EscrowWallet({ privateKey: "c1280d4b8f06d0d2b02b032d597e02dcdc6afa29b50526027f057aa9118b8187" });

module.exports = {
    creatorWallet,
    brokerWallet,
    recipientWallet,
};