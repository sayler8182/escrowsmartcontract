const { Escrow } = require("../src/Escrow");
const assert = require("assert");
const {
    creatorWallet,
    brokerWallet,
    recipientWallet,
} = require("./config");

// default escrow config
const defaultEscrowConfig = {
    brokerAddress: brokerWallet.getAddress(),
    recipientAddress: recipientWallet.getAddress(),
    amount: 1,
    fee: 1,
    notes: "Some note",
};

// assert revert
const assertReverts = async (promise, expectedError, details) => {
    try {
        await promise;
        assert.fail("Transaction did not revert.");
    } catch (error) {
        const _details = details && ` -> ${details}`;
        if (expectedError) {
            assert.equal(error.message.includes(expectedError), true, `${error.message}${_details}`);
        } else {
            assert.equal(error.message.includes("VM Exception while processing transaction: revert"), true, `${error.message}${_details}`);
        }
    }
}

// start escrow
const startEscrow = async (wallet, { contractAddress, brokerAddress, recipientAddress, amount, fee, notes }) => {
    const escrow = new Escrow(wallet, { contractAddress });
    await escrow.startEscrow({
        brokerAddress,
        recipientAddress,
        amount,
        fee,
        notes,
    });
    return escrow;
};

// open new default escrow
const openNewDefaultEscrow = async (creatorWallet, userWallet) => {
    let escrow = new Escrow(creatorWallet);
    await escrow.startEscrow({
        brokerAddress: defaultEscrowConfig.brokerAddress,
        recipientAddress: defaultEscrowConfig.recipientAddress,
        amount: defaultEscrowConfig.amount,
        fee: defaultEscrowConfig.fee,
        notes: defaultEscrowConfig.notes,
    });
    return new Escrow(userWallet, { contractAddress: escrow.getAddress() });
};

// open escrow
const openEscrow = (userWallet, escrow) => {
    return new Escrow(userWallet, { contractAddress: escrow.getAddress() });
};

// check actions
const checkActions = async (escrow, { accept, cancel, confirm, dispute, confirmDispute, rejectDispute } = {}, user) => {
    if (!accept) {
        await assertReverts(escrow.accept(), "VM Exception while processing transaction: revert", `accept by ${user}`);
    }
    if (!cancel) {
        await assertReverts(escrow.cancel(), "VM Exception while processing transaction: revert", `cancel by ${user}`);
    }
    if (!confirm) {
        await assertReverts(escrow.confirm(), "VM Exception while processing transaction: revert", `confirm by ${user}`);
    }
    if (!dispute) {
        await assertReverts(escrow.dispute(), "VM Exception while processing transaction: revert", `dispute by ${user}`);
    }
    if (!confirmDispute) {
        await assertReverts(escrow.confirmDispute(), "VM Exception while processing transaction: revert", `confirmDispute by ${user}`);
    }
    if (!rejectDispute) {
        await assertReverts(escrow.rejectDispute(), "VM Exception while processing transaction: revert", `rejectDispute by ${user}`);
    }
};

// check withdraw
const checkWithdraw = async (escrow, { amount = null, withdraw } = {}, user) => {
    if (amount != null) {
        const availableAmount = await escrow.availableWithdraw();
        assert.equal(availableAmount, amount, `Available withdraw is incorrect by ${user}: ${availableAmount} != ${amount}`);
    }
    if (amount == null) {
        await assertReverts(escrow.availableWithdraw(), "VM Exception while processing transaction: revert", `availableWithdraw by ${user}`);
    }
    if (withdraw) {
        await assertReverts(escrow.withdraw(), "VM Exception while processing transaction: revert", `withdraw by ${user}`);
    }
};

// check contract state
const checkContractState = async (escrow, { creator, broker, recipient } = {}) => {
    const creatorEscrow = openEscrow(creatorWallet, escrow);
    await checkActions(creatorEscrow, creator, "creator");
    await checkWithdraw(creatorEscrow, creator, "creator");

    const brokerEscrow = openEscrow(brokerWallet, escrow);
    await checkActions(brokerEscrow, broker, "broker");
    await checkWithdraw(brokerEscrow, broker, "broker");

    const recipientEscrow = openEscrow(recipientWallet, escrow);
    await checkActions(recipientEscrow, recipient, "recipient");
    await checkWithdraw(recipientEscrow, recipient, "recipient");
};

// amount without fee
const amountWithoutFee = () => {
    return defaultEscrowConfig.amount - (defaultEscrowConfig.amount * defaultEscrowConfig.fee / 100);
};

// amount with
const amountWithFee = () => {
    return defaultEscrowConfig.amount;
};

// amount fee
const amountFee = () => {
    return defaultEscrowConfig.amount * defaultEscrowConfig.fee / 100;
};

module.exports = {
    defaultEscrowConfig,
    assertReverts,
    startEscrow,
    openNewDefaultEscrow,
    openEscrow,
    checkActions,
    checkContractState,
    amountWithoutFee,
    amountWithFee,
    amountFee,
};